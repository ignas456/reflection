﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionProject.Services
{
    public class MockOpenDialogService : IOpenDialogService
    {

        public string Path { get; set; }
        public string GetFilePath()
        {
            return Path;
        }
    }
}
