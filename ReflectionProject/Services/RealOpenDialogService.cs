﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionProject.Services
{
    class RealOpenDialogService : IOpenDialogService
    {
        public string GetFilePath()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "DLL|*.dll|Executable|*.exe";
            if (dlg.ShowDialog().GetValueOrDefault())
            {
                return dlg.FileName;
            }
            return null;
        }
    }
}
