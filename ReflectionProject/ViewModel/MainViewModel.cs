﻿using ReflectionProject.Model;
using ReflectionProject.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;

namespace ReflectionProject.ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Constructors
        public MainViewModel(IOpenDialogService fileService)
        {
            fileDialog = fileService;
            ReadDllFileCommand = new RelayCommand(p => ReadDllFile());

            _assembly = new AssemblyModel();
        }
        #endregion

        #region Fields and Properties

        private IOpenDialogService fileDialog;
        private AssemblyModel _assembly;
        private AssemblyModel _tree;
        
        public Model.AssemblyModel _Assembly
        {
            get { return _assembly; }
            set
            {
                _assembly = value;
                OnPropertyChanged("_Assembly");
            }
        }
        public Model.AssemblyModel Tree
        {
            get { return _tree; }
            set
            {
                _tree = value;
                OnPropertyChanged("Tree");
            }
        }
        #endregion

        #region Commands
        public ICommand ReadDllFileCommand { get; private set; }
        public ICommand SerializeToXmlCommand { get; private set; }
        public ICommand ReadXmlFileCommand { get; set; }



        public void ReadDllFile()
        {
            _assembly.AnalyseAssembly(fileDialog.GetFilePath());
            Tree = new AssemblyModel() { Name = _assembly.Name };
            foreach (NamespaceModel n in _assembly.Namespaces)
            {
                NamespaceModel temp = new NamespaceModel() { Name = n.Name };
                Tree.Namespaces.Add(temp);
            }
        }
        private void SerializeToXml()
        {
           
        }
        private void ReadXmlFile()
        {

        }
        #endregion

        #region Expanding and Collapsing treeview nodes

        public void Expand(object node)
        {
            if (node.GetType() == typeof(NamespaceModel))
            {
                NamespaceModel element = (NamespaceModel)node;
                foreach (ClassModel _class in LoadNamespaceInfo(element))
                {
                    element.Classes.Add(new ClassModel() 
                    { Name = _class.Name, Namespace = _class.Namespace, Description = "Class: " });
                }
            }
            else
            {
                if (node.GetType() == typeof(ClassModel))
                {
                    ClassModel element = (ClassModel)node;
                    LoadClassInfo(element);
                }
            }
        }

        public void Collapse(object node)
        {
            if (node.GetType() == typeof(NamespaceModel))
            {
                NamespaceModel element = (NamespaceModel)node;
                element.Classes.Clear();
            }
            else
            {
                if (node.GetType() == typeof(ClassModel))
                {
                    ClassModel element = (ClassModel)node;
                    element.BaseType = null;
                    element.Interfaces.Clear();
                    element.Fields.Clear();
                    element.Properties.Clear();
                    element.Methods.Clear();
                }
            }
        }

        //Methods returning classes in namespace.
        private ObservableCollection<ClassModel> LoadNamespaceInfo(NamespaceModel _namespace)
        {
            NamespaceModel nmspc = _assembly.Namespaces.First(p => p.Name == _namespace.Name);
            return nmspc.Classes;
        }

        //Method saving info about class.
        
        private void LoadClassInfo(ClassModel _class)
        {
            ClassModel clss = _assembly._classDictionary[_class.Name];

            //Saving BaseType
            if (clss.BaseType != null)
                _class.BaseType = new ClassModel()
                {
                    Name = clss.BaseType.Name,
                    Namespace = clss.BaseType.Namespace,
                    Description = "BaseType: "
                };

            //Saving implemented interfaces
            foreach (ClassModel _interface in clss.Interfaces)
            {
                _class.Interfaces.Add(new ClassModel()
                {
                    Name = _interface.Name,
                    Namespace = _interface.Namespace,
                    Description = "Interface: "
                });
            }

            //Saving fields
            foreach (ClassModel field in clss.Fields)
            {
                _class.Fields.Add(new ClassModel()
                {
                    Name = field.Name,
                    Namespace = field.Namespace,
                    Description = "Field: "
                });
            }

            //Saving properties
            foreach (ClassModel property in clss.Properties)
            {
                _class.Properties.Add(new ClassModel()
                {
                    Name = property.Name,
                    Namespace = property.Namespace,
                    Description = "Property: "
                });
            }

            //Saving methods with ReturnType and Parameters info (generating two levels in tree)
            foreach (MethodModel method in clss.Methods)
            {
                MethodModel meth = new MethodModel() { Name = method.Name };

                meth.ReturnType = new ClassModel()
                {
                    Name = method.ReturnType.Name,
                    Namespace = method.ReturnType.Namespace,
                    Description = "Return Type: "
                };
                foreach (ClassModel pars in method.ParameterList)
                {
                    meth.ParameterList.Add(new ClassModel()
                    {
                        Name = pars.Name,
                        Namespace = pars.Namespace,
                        Description = "Parameter: "
                    });
                }
                _class.Methods.Add(meth);
            }
        }
        #endregion


        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged = null;
        virtual protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion



    }
}
