﻿using ReflectionProject.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReflectionProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new ViewModel.MainViewModel(new RealOpenDialogService());
        }

        private void Expand(object sender, RoutedEventArgs e)
        {
            TreeViewItem expandedNode = e.OriginalSource as TreeViewItem;
            var viewModel = this.DataContext as ViewModel.MainViewModel;
            viewModel.Expand(expandedNode.DataContext);
        }

        private void Collapsed(object sender, RoutedEventArgs e)
        {
            TreeViewItem collapsedNode = e.OriginalSource as TreeViewItem;
            var viewModel = this.DataContext as ViewModel.MainViewModel;
            viewModel.Collapse(collapsedNode.DataContext);
        }
    }
}
