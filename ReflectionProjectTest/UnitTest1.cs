﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReflectionProject;
using ReflectionProject.Model;
using System.Reflection;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using ReflectionProject.Services;
using ReflectionProject.ViewModel;
using System.Collections.Generic;



namespace ReflectionProjectTest
{
    [TestClass]
    public class UnitTest1
    {
        private Assembly _assembly;
        private AssemblyModel _objAssembly;
        private ClassModel _classModel;
        private MethodModel _methodModel;

        [TestMethod]
        public void TestReadDllFile()
        {
           MockOpenDialogService mockOpenDialogService = new MockOpenDialogService() 
            { 
                Path = Path.Combine(Path.GetDirectoryName(Path.GetDirectoryName
               (System.IO.Directory.GetCurrentDirectory())), @"EntityFramework.dll") 
            };

            MainViewModel mainViewModel = new MainViewModel(mockOpenDialogService);
            
            mainViewModel.ReadDllFile();
            Assert.IsNotNull(mainViewModel.Tree);      
        }

        [TestMethod]
        public void TestAnalyseAssembly()
        {
            _objAssembly = new AssemblyModel();

            String path = Path.Combine(Path.GetDirectoryName(Path.GetDirectoryName
               (System.IO.Directory.GetCurrentDirectory())), @"EntityFramework.dll");

            _objAssembly.AnalyseAssembly(path);
            Assert.IsNotNull(_objAssembly.Name);
         }

        [TestMethod]
        public void TestAnalyseClass()
        {
            Type type = this.GetType();
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string,ClassModel>();
            _classModel = new ClassModel();
            _classModel.AnalyseClass(type, classDictionary);
            Assert.IsNotNull(_classModel.BaseType);
        }

        [TestMethod]
        public void TestGetBaseType()
        {
            Type type = this.GetType();
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string, ClassModel>();
            _classModel = new ClassModel();
           
            ClassModel baseType = _classModel.GetBaseType(type, classDictionary);
            Assert.IsNotNull(baseType);
        }

        [TestMethod]
        public void TestGetInterfaces()
        {
            Type type = new ClassModel().GetType();
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string, ClassModel>();
            _classModel = new ClassModel();

            _classModel.GetInterfaces(type, classDictionary);
            Assert.IsNotNull(_classModel.Interfaces);
        }

        [TestMethod]
        public void TestGetFields()
        {
            Type type = this.GetType();
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string, ClassModel>();
            _classModel = new ClassModel();

            _classModel.GetFields(type, classDictionary);
            Assert.IsNotNull(_classModel.Fields);
        }

        [TestMethod]
        public void TestGetProperties()
        {
            Type type = this.GetType();
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string, ClassModel>();
            _classModel = new ClassModel();

           _classModel.GetProperties(type, classDictionary);
            Assert.IsNotNull(_classModel.Properties);
        }

        [TestMethod]
        public void TestGetMethods()
        {
            Type type = this.GetType();
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string, ClassModel>();
            _classModel = new ClassModel();

            _classModel.GetMethods(type, classDictionary);
            Assert.IsNotNull(_classModel.Methods);
        }

        [TestMethod]
        public void TestAnalyseMethod()
        {
            Type type = this.GetType();
            MethodInfo method = type.GetMethod("TestAnalyseMethod");
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string, ClassModel>();
            _methodModel = new MethodModel();

            _methodModel.AnalyseMethod(method, classDictionary);
            Assert.IsNotNull(_methodModel.Name);
        }

        [TestMethod]
        public void TestGetReturnType()
        {
            Type type = this.GetType();
            MethodInfo method = type.GetMethod("TestGetReturnType");
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string, ClassModel>();
            _methodModel = new MethodModel();

            ClassModel returnType = _methodModel.GetReturnType(method, classDictionary);
            Assert.IsNotNull(returnType);
        }

        [TestMethod]
        public void TestGetParameterList()
        {
            Type type = this.GetType();
            MethodInfo method = type.GetMethod("TestGetParameterList");
            Dictionary<string, ClassModel> classDictionary = new Dictionary<string, ClassModel>();
            _methodModel = new MethodModel();

            _methodModel.GetParameterList(method, classDictionary);
            Assert.IsNotNull(_methodModel.ParameterList);
        }
        
        
    }
}
